<?php

/**
 * Create a basic sitemap.xml file to help site get indexed
 */

declare(strict_types=1);

require_once __DIR__ . '/../../boot.php';

updateSitemap();

podLog('sitemap.xml updated');
addMeta('sitemap_updated');
