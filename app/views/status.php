<?php

declare(strict_types=1);

require_once __DIR__ . '/../../boot.php';

use Carbon\Carbon;

$pods_updated_stats = Carbon::createFromFormat('Y-m-d H:i:s.u', getMeta('pods_updated', 'date_created'))->locale($locale->language)->diffInMinutes();
echo "<div class='medium ms-3 mb-5 mt-5'>";
if ($pods_updated_stats < $_SERVER['STATUS_GREEN']) {
    echo $t->trans('base.strings.status.current') . ' <div class="text-success d-inline-block">' . $t->trans('base.strings.status.green') . '</div> ' . $t->trans('base.strings.status.status') . ' ';
} else {
    echo $t->trans('base.strings.status.current') . ' <div class="text-danger d-inline-block">' . $t->trans('base.strings.status.red') . '</div> ' . $t->trans('base.strings.status.status') . ' ';
}
echo getMeta('pods_updating') ? $t->trans('base.strings.status.running') : $t->trans('base.strings.status.idle');
echo "<br><br>";
$pods_updated_stats_human = Carbon::createFromFormat('Y-m-d H:i:s.u', getMeta('pods_updated', 'date_created'))->locale($locale->language)->diffForHumans();

echo $t->trans('base.strings.status.last') . ' ' . allDomainsData(null, true)[0]['count'] . ' ' . $t->trans('base.general.servers') . '<br>';

echo $t->trans('base.strings.status.update') . ' '  . $pods_updated_stats_human . "<br>";

$last_runtime_human = secondsToTime(getMeta("pods_update_runtime") * 60);

echo $t->trans('base.strings.status.updatetook') . ' '  . $last_runtime_human . " <br>";

$lang_updated_stats = Carbon::createFromFormat('Y-m-d H:i:s.u', getMeta('languages_updated', 'date_created'))->locale($locale->language)->diffForHumans();
echo $t->trans('base.strings.status.language') . ' '  . $lang_updated_stats . "<br>";

$back_updated_stats = Carbon::createFromFormat('Y-m-d H:i:s.u', getMeta('backup', 'date_created'))->locale($locale->language)->diffForHumans();
echo $t->trans('base.strings.status.backup') . ' '  . $back_updated_stats . "<br>";

$masterversions_updated_stats = Carbon::createFromFormat('Y-m-d H:i:s.u', getMeta('masterversions_updated', 'date_created'))->locale($locale->language)->diffForHumans();
echo $t->trans('base.strings.status.masterversion') . ' '  . $masterversions_updated_stats . "<br>";

$federation_updated_stats = Carbon::createFromFormat('Y-m-d H:i:s.u', getMeta('federation_updated', 'date_created'))->locale($locale->language)->diffForHumans();
echo $t->trans('base.strings.status.crawl') . ' '  . $federation_updated_stats . "<br>";

$statstable_updated_stats = Carbon::createFromFormat('Y-m-d H:i:s.u', getMeta('statstable_updated', 'date_created'))->locale($locale->language)->diffForHumans();
echo $t->trans('base.strings.status.monthly') . ' '  . $statstable_updated_stats . "<br>";

echo "</div>";
