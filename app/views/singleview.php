<?php

/**
 * This is just a single pod data pull for /domain.tld page
 */

declare(strict_types=1);

require_once __DIR__ . '/../../boot.php';

use RedBeanPHP\R;
use Carbon\Carbon;
use RedBeanPHP\RedException;

$input = isset($_GET['input']) ? substr($_GET['input'], 1) : null;
$hiddensoftwares = txtToQuery($_SERVER['SOFTWARE_BLACKLIST']);
$hiddendomains   = txtToQuery($_SERVER['DOMAINS_BLACKLIST']);

// Required parameters.
if (isset($_GET['domain'])) {
    $_domain = $_GET['domain'];
    $utf8domain = idn_to_utf8($_GET['domain']);
} elseif ($input != null) {
    $_domain = $input;
    $utf8domain = idn_to_utf8($input);
} else {
    die('no domain given');
}

$iso = new Matriphe\ISO639\ISO639();

try {
    $pod = R::getRow('
        SELECT domain, softwarename, signup, daysmonitored, score, name, countryname, city, state, detectedlanguage, date_created, date_laststats, status, metatitle, metadescription, masterversion, shortversion
        FROM pods
        WHERE domain = ?
        AND softwarename NOT SIMILAR TO ?
        AND domain NOT SIMILAR TO ?
    ', [$_domain, $hiddensoftwares, $hiddendomains]);
} catch (RedException $e) {
    die('Error in SQL query: ' . $e->getMessage());
}

try {
    $domain_clicks = R::getAll("
        SELECT
            to_char(date_clicked, 'yyyy-mm') AS yymm,
            SUM(manualclick) AS manualclick, 
            SUM(autoclick) AS autoclick
        FROM clicks
        WHERE domain = ?
        GROUP BY yymm
        ORDER BY yymm
    ", [$_domain]);
} catch (RedException $e) {
    die('Error in SQL query: ' . $e->getMessage());
}
if ($pod['domain']) {
    $humanmonitored = Carbon::createFromTimeStamp(strtotime($pod['date_created']))->locale($locale->language)->diffForHumans();
    echo '<div class="container">';
    echo '<div class="text-justify row row-cols-1 p-2 w-100"><h5><a href="/go&domain=' . $pod['domain'] . '">' . $pod['metatitle'] . '</a></h5>';
    echo '<p class="fw-bold">' . $pod['metadescription'] . '</p>';
    echo $t->trans('base.strings.singlepage.version', ['%(domain)' => $pod['domain'], '%(software)' => $pod['softwarename'], '%(version)' => $pod['shortversion'], '%(masterversion)' => $pod['masterversion']]);
    echo '<br>';
    echo $t->trans('base.strings.singlepage.status', ['%(daysmonitored)' => $humanmonitored, '%(score)' => $pod['score'], '%(maxscore)' => '100']);
    echo '<br>';
    echo $t->trans('base.strings.singlepage.language', ['%(language)' => ($pod['detectedlanguage'] ? $iso->languageByCode1($pod['detectedlanguage']) : ''), '%(location)' => $pod['countryname']]);
    echo '<br>';
    $last_podcheck  = Carbon::createFromFormat('Y-m-d H:i:s', $pod['date_laststats'])->locale($locale->language)->diffForHumans(null, true);
    echo '<br>' . $t->trans('base.strings.singlepage.lastchecked') . ' ' . $last_podcheck;
    echo ' ago. <br><br>';
    echo ($pod['signup'] ? $t->trans('base.strings.singlepage.opensignup') : $t->trans('base.strings.singlepage.closedsignup'));
    echo '</p></div>';
    echo '<div class="w-100 p-2 row"></div>';
    $_GET['domain'] = $pod['domain'];
    echo  '<div class="align-items-center row"><h5 class="fw-bold text-center">' . $t->trans('base.strings.singlepage.uptime') . '</h5></div><div class="d-flex w-100 align-items-center row">';
    include 'podstat-uptime.php';
    echo '</div><div class="align-items-center row"><h5 class="fw-bold text-center">' . $t->trans('base.strings.singlepage.userstats') . '</h5></div><div class="d-flex w-100 align-items-center row">';
    include 'podstat-counts.php';
    ?>
    </div>
    <div class="align-items-center row">
    <h5 class="fw-bold text-center"><?php echo $t->trans('base.strings.singlepage.clicksout') ?></h5>
    </div>
    <div class="align-items-center row">
        <div class="d-flex w-100 chart-container p-1">
            <canvas id="clicks"></canvas>
        </div>
    </div>
</div>
<script>
    new Chart(document.getElementById('clicks'), {
        type: "bar",
        data: {
            labels: <?php echo json_encode(array_column($domain_clicks, 'yymm')); ?>,
            datasets: [{
                data: <?php echo json_encode(array_column($domain_clicks, 'manualclick')); ?>,
                label: 'Manual',
                fill: false,
                yAxisID: "l2",
                borderColor: "#A07614",
                backgroundColor: "#A07614",
                borderWidth: 4,
                pointHoverRadius: 6
            }, {
                data: <?php echo json_encode(array_column($domain_clicks, 'autoclick')); ?>,
                label: 'Auto',
                fill: false,
                yAxisID: "l2",
                borderColor: "#4b6588",
                backgroundColor: "#4b6588",
                borderWidth: 4,
                pointHoverRadius: 6
            }]
        },
        options: {
            responsive: true,
            maintainAspectRatio: true,
            scales: {
                l2: {
                    position: "left",
                }
            }
        }
    });
    </script>
    <?php
} else {
    podLog('Singlepage Missing', $_domain, 'warning');
    ?>
    <div class="container">
        <div class="text-justify row row-cols-1 p-2">
        <h1><?php echo $t->trans('base.strings.singlepage.notfound') ?></h1>
            <a href="/?domain=<?php echo $_domain ?>&add=&action=save"><?php echo $t->trans('base.strings.singlepage.addit') ?></a>
        </div>
    </div>
    <?php
}
