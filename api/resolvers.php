<?php

use RedBeanPHP\R;
use Poduptime\PodStatus;
use function Siler\{array_get_str};

require_once __DIR__ . '/../boot.php';

$queryType = [
    'nodes' => function ($_, array $args) {
        $sql = '
        SELECT 
         id,
         domain,
         name,
         metatitle,
         metadescription,
         metanodeinfo,
         metalocation,
         owner,
         metaimage,
         onion,
         i2p,
         terms,
         pp,
         support,
         camo,
         zipcode,
         softwarename,
         masterversion,
         fullversion,
         shortversion,
         score,
         ip,
         detectedlanguage,
         country,
         countryname,
         city,
         state, 
         lat,
         long,
         ipv6,
         sslvalid,
         monthsmonitored,
         daysmonitored,
         signup,
         total_users, 
         active_users_halfyear,
         active_users_monthly,
         local_posts,
         uptime_alltime,
         status,
         latency,
         service_xmpp,
         services,
         protocols,
         podmin_statement,
         podmin_notify,
         podmin_notify_level,
         sslexpire,
         dnssec,
         comment_counts,
         weight,
         date_updated,
         date_laststats,
         date_created
        FROM pods
    ';
        $softwarename = array_get_str($args, 'softwarename', '');
        $status       = PodStatus::getAll()[array_get_str($args, 'status', '')] ?? null;
        if ($softwarename !== '' && isset($status)) {
            $sql .= ' WHERE softwarename = ? AND status = ?';
            return R::getAll($sql, [$softwarename, $status]);
        }
        if ($softwarename !== '') {
            $sql  .= ' WHERE softwarename = ?';
            return R::getAll($sql, [$softwarename]);
        }
        if (isset($status)) {
            $sql .= ' WHERE status = ?';
            return R::getAll($sql, [$status]);
        }
        return R::getAll($sql);
    },
    'node' => function ($_, array $args) {
        $domain = array_get_str($args, 'domain');
        return R::getAll('
        SELECT 
         id,
         domain,
         name,
         metatitle,
         metadescription,
         metanodeinfo,
         metalocation,
         owner,
         metaimage,
         onion,
         i2p,
         terms,
         pp,
         support,
         camo,
         zipcode,
         softwarename,
         masterversion,
         fullversion,
         shortversion,
         score,
         ip,
         detectedlanguage,
         country,
         countryname,
         city,
         state, 
         lat,
         long,
         ipv6,
         sslvalid,
         monthsmonitored,
         daysmonitored,
         signup,
         total_users, 
         active_users_halfyear,
         active_users_monthly,
         local_posts,
         uptime_alltime,
         status,
         latency,
         service_xmpp,
         services,
         protocols,
         podmin_statement,
         podmin_notify,
         podmin_notify_level,
         sslexpire,
         dnssec,
         comment_counts,
         weight,
         date_updated,
         date_laststats,
         date_created
        FROM pods
        WHERE domain = ?
    ', [$domain]);
    },
    'checks' => function ($_, array $args) {
        $limit = $args ? array_get_str($args, 'limit') : '5';
        return R::getAll('
        SELECT id, domain, online, error, latency, total_users, local_posts, comment_counts, shortversion, version, date_checked
        FROM checks 
        ORDER BY date_checked DESC
        LIMIT ?
    ', [$limit]);
    },
    'clicks' => function ($_, array $args) {
        $limit = $args ? array_get_str($args, 'limit') : '5';
        return R::getAll('
        SELECT id, domain, manualclick, autoclick, date_clicked
        FROM clicks
        ORDER BY date_clicked DESC
        LIMIT ?
    ', [$limit]);
    },
    'monthlystats' => function () {
        return R::getAll('
        SELECT id, softwarename, total_users, total_posts, total_comments, total_pods, total_uptime, date_checked
        FROM monthlystats
        ORDER BY id DESC
    ');
    },
    'masterversions' => function () {
        return R::getAll('
        SELECT id, software, version, devlastcommit, releasedate, date_checked
        FROM masterversions
        ORDER BY id DESC
    ');
    },
];

return [
    'Query'      => $queryType,
];
