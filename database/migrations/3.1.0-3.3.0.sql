ALTER TABLE pods DROP COLUMN stats_apikey, DROP COLUMN userrating;
DROP table ratingcomments;
DROP table apikeys;
DELETE FROM meta where name = 'wizard_data';
DELETE FROM meta where name = 'browser_data';
DELETE FROM meta where name = 'search_query';
UPDATE pods SET ipv6 = null;
ALTER TABLE pods ALTER COLUMN ipv6 TYPE text;
